export default class {
  static async run(page, req, context) {
    try {
      let eaccs = (req.session_data.cfg.emails) ? req.session_data.cfg.emails : []; 

      context.no_unread_emails = 0;
      for (let a = 0; a < eaccs.length; a++) {
        let acc = eaccs[a];
        let esess = page.app.cms.mailer.get_session(req.session_data.id, acc.user);
        context.no_unread_emails += await esess.imap.get_unread_count();
      }
      

    } catch (e) {
      console.error(e.stack);
    }
  }
}

