

import bcrypt from 'bcryptjs';
const redirect_path = "/admin_auth";

export default class Admins {
  static async construct(cms) {
    try {
      console.log("+", "Admins");

      let _this = new Admins(cms);

      console.log("ADMIN API");

      cms.app.post("/admin-api", cms.admin.auth.orize, async function(req, res, next) {
        try {
          var data = req.body;

          if (data.data) {
            if (typeof data.data === 'string' || data.data instanceof String) {
              data = JSON.parse(data.data);
            }
          }

          req.data = data;

          switch (data.command) {
            case 'details':
              _this.details(req, res, next);
              break;
            case 'change-password':
              _this.chpwd(req, res, next);
              break;
            default:
          }
        } catch (e) {
          console.error(e.stack);
        }
      });
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cms) {
    this.terminate = cms.admin.auth.terminate;
    this.table = cms.admin.auth.table;
    this.session_table = cms.admin.auth.session_table;
  }


  async details(req, res, next) {
    try {
      let found = await this.table.select(
        '*', 'id = $1', [req.session_data.id]
      );


      if (found.length > 0) {
        found = found[0];
        delete found.password;
        delete found.secret;
        res.send(JSON.stringify(found));
      } else {
        this.terminate(req, res, next);
      }

    } catch (e) {
      console.error(e.stack);
    }
  }

  async chpwd(req, res, next) {
    try {
      var data = JSON.parse(req.body.data);
      console.log("data", data);
      var found = await this.table.select(
        '*',
        "id = $1", [req.session_data.id]
      );
      
      if (found.length > 0) {
        found = found[0];
        if (bcrypt.compareSync(data.upwd, found.password)) {
          if (data.npwd1 == data.npwd2) {
            var salt = bcrypt.genSaltSync(10);
            await this.table.update({
              password: bcrypt.hashSync(data.npwd1, salt)
            }, "id = $1", [found.id]);
            res.json({});
          } else {
            res.json({ err: "Passwords don't match!" });
          }
        } else {
          res.json({ err: "Incorrect password!" });
        }
      } else {
        this.terminate(req, res, next);
      }

    } catch (e) {
      console.error(e.stack);
    }
  }
}
