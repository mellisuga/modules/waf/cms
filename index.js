import path from 'path';
import fs from 'fs';

//import PostsIO from './posts/index.js';
//import Gallery from './gallery/index.js';
//import Users from './users/index.js';
import Admins from './admins/index.js';
/**
 * Main class for the whole library.
 * @class ContentManager
 * @hideconstructor
 */
export default class CMS {
  
  constructor(cms, cfg) {
    this.cms = cms;

    this.root_page = cfg.root_page;
    
  }
  /**
   * @static
   * @async
   * @method construct
   * @memberof ContentManager
   * @arg {Mellisuga} cms CMS object
   * @arg {Object} cfg Configuration 
   * @arg {Object} [cfg.context] Extra context for pages
   * @arg {String[]} [cfg.disabled_pages] Default pages to be disabled
   * @returns {ContentManager}
   */
  static async construct(cms, cfg) {
    try {
      console.log("CONTENT MANAGER:");
      console.debug("CFG:", cfg);
      let this_class = new CMS(cms, cfg);
      let _this = this_class;
      let posts = undefined;
      //if (!cfg.disabled_pages.includes("Posts")) posts = this_class.posts = await PostsIO.init(cms, cfg.posts);

      let gallery = undefined;
      /**
        @name ContentManager#gallery
        @type {CMSGallery}
      */
      //if (!cfg.disabled_pages.includes("Gallery")) gallery = this_class.gallery = await Gallery.init(cms, cfg.gallery);
      let users = undefined;
      //if (!cfg.disabled_pages.includes("Users")) users = this_class.users = await Users.init(cms);
      let admins = undefined;
      admins = _this.admins = await Admins.construct(cms);


      const lang_json = fs.readFileSync(
        path.resolve(new URL('.', import.meta.url).pathname, "lang", (cfg.lang || "en")+".json"),
        "utf8"
      );
      let lang = JSON.parse(lang_json);

      _this.path = cfg.path || "/cms-admin";
      
      let dir_cfg = {
        auth: cms.admin.auth,
        globals_path: cfg.globals || path.resolve(new URL('.', import.meta.url).pathname, 'globals'),
        name: "content management",
        required_rights: [ "content_management" ],
        context: {
          cms_path: cfg.path || "/cms-admin",
          lang: lang,
          custom: cfg.context,
          maildomain: cfg.maildomain
        },
        aliases: {
          globals: path.resolve(new URL('.', import.meta.url).pathname, "globals/modules")
        }
      }
      if (lang) {
        dir_cfg.custom_names = lang.page_names
      }

      this_class.globals_path = dir_cfg.globals_path;



      if (cfg.disabled_pages) dir_cfg.disabled_pages = cfg.disabled_pages;
      if (cfg.dev_mode) cms.pages.watch_less(path.resolve(dir_cfg.globals_path, "theme.less"));
      this_class.page_list = cms.pages.serve_dirs(cfg.path || "/cms-admin", cfg.dist || path.resolve(new URL('.', import.meta.url).pathname, 'dist'), dir_cfg);

      cms.cms = cms.content_manager = _this;
      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  /**
   * @method serve_extra_page
   * @memberof ContentManager
   * @arg {String} name Name of the new page
   * @arg {String} path Path to the page directory
   * @returns {Page}
   */

  serve_extra_page(name, path, cfg) {
    if (!cfg) cfg = {};
    console.log("SERVE EXTRA", path);
    let npage_path = this.path+"/"+name;
    if (name === this.root_page) npage_path = this.path;
    let cms = this.cms;
    let page = cms.pages.serve_dir(npage_path, path, {
      auth: cms.admin.auth,
      name: name,
      globals_path: cms.content_manager.globals_path,
      required_rights: [ "content_management" ],
      context: cms.content_manager.page_list.context,
      global_context_path: cms.content_manager.page_list.global_context_path,
      disabled_pages: cms.content_manager.page_list.disabled_pages,
      parent_list: this.page_list
    });

    if (cfg.add_to_menu) {
      let extra_cm_ctx = {
        menu: []
      };
      extra_cm_ctx.menu.push({
        name: name,
        path: "/"+name
      });
  //    page.add_extra_context(extra_cm_ctx);
      this.page_list.add_extra_context(extra_cm_ctx);
    }

    return page;
  }
}

