
import XHR from 'core/utils/xhr_async.js';

import ContentNavigator from 'core/content_navigator/index.js';

const content_elem = document.getElementById("content");

import MailPage from './mail_page.js';

import top_opts_html from './top-options.html';


import fingerprint from 'core/flyauth.ui/fingerprint/index.js';
import FlyAuth from 'core/flyauth.ui/index.js';

let new_email_counter = document.getElementById("new_email_counter");


(async function() {
  try {
    let _this = new (class {
      constructor() {
        this.cur_eaddr = "mellisuga-mailbot@subtledev.space";
        this.max_index = 100;
      }
    })();

    await require("globals/header.js").construct();

    let addrs = await XHR.post("/content-manager/mailer.io", {
      command: "all_addrs"
    }, "access_token");
    console.log(addrs);

    let addrse = document.getElementById("email_addrs");

    let last_selected_addr = false;
    for (let a = 0; a < addrs.length; a++) {
      let addr_button = document.createElement("button");
      if (!last_selected_addr) last_selected_addr = addr_button;
      addr_button.innerHTML = addrs[a];
      addr_button.addEventListener("click", async function() {
        try {
          display_loading_overlay();
          console.log("fch", mboxe.firstChild);
          mboxe.firstChild.classList.add("selected");
          if (displayed_box_btn != mboxe.firstChild) {
            displayed_box_btn.classList.remove("selected");
            displayed_box_btn = mboxe.firstChild;
          }
          wemaile.style.display = "";
          content_elem.style.display = "";

          _this.cur_eaddr = addrs[a];
          last_selected_addr.classList.remove("selected");
          addr_button.classList.add("selected");
          last_selected_addr = addr_button;
          _this.curopenbox = await open_mailbox("INBOX");
          await navigator.resize(10, _this.max_index);
          await navigator.get_page(0, navigator.page_size);
        } catch (e) {
          console.error(e.stack);
        }
      });
      

      
      addrse.appendChild(addr_button);
    }

    addrse.firstChild.classList.add("selected");;

    let mboxe = document.getElementById("mailboxes");
    let mailboxes = await XHR.post("/content-manager/mailer.io", {
      command: "get_boxes",
      addr: _this.cur_eaddr
    }, "access_token");
    mailboxes.sort(function(a, b) {
      if (a.toLowerCase() === "inbox") {
        return -1;
      } else if (b.toLowerCase() === "inbox") {
        return 1;
      } else if (a.toLowerCase() === "sent") {
        return -1;
      } else if (b.toLowerCase() === "sent") {
        return 1;
      } else {
        return 0;
      }
      
    });

    let open_boxes = [];

    async function open_mailbox(mailbox_name) {
      try {
        let this_box = { name: mailbox_name };

        this_box.hide = function() {
          elem.style.display = "none";
        }
        this_box.display = function() {
          elem.style.display = ""
        }

        _this.max_index = await XHR.post("/content-manager/mailer.io", {
          command: "inbox_max",
          addr: _this.cur_eaddr,
          boxname: mailbox_name 
        }, "access_token");

        console.log(_this.max_index);

        let mail_page = undefined;
        return this_box;
      } catch (e) {
        console.error(e.stack);
      }
    }

/*    await XHR.post("/content-manager/mailer.io", {
      command: "open_box",
      name: "INBOX",
      addr: _this.cur_eaddr,
              boxname: _this.curopenbox.name
    }, "access_token");*/
    _this.curopenbox = await open_mailbox("INBOX");
    open_boxes.push(_this.curopenbox);
    _this.cur_eaddr = addrs[0];


    console.log("curopenbox", _this.curopenbox);


    let wemaile = document.getElementById("write_email");

    let wemaile_form = document.getElementById("write_email_form");
    let wemaile_to = wemaile_form.querySelector("input:nth-child(1)");
    let wemaile_subject = wemaile_form.querySelector("input:nth-child(2)");
    let wemaile_content = wemaile_form.querySelector("textarea");

    let attach_div = wemaile_form.querySelector("div.attachments");
    let wemaile_attach = wemaile_form.querySelector('button.attach');

    let attach_form = document.createElement('form');
    attach_form.style.display = "none";
    let upload_input = document.createElement("input");
    upload_input.type = "file";
    upload_input.name = "attachments";
    upload_input.multiple = "multiple";
    attach_form.appendChild(upload_input);
    document.body.appendChild(attach_form);


    let submit_func = async function(ev) {
      try {
        ev.preventDefault();
        attach_div.innerHTML = "";
        for (let f = 0; f < this.files.length; f++) {
          let fdiv = document.createElement("div");
          fdiv.innerHTML = this.files[f].name;
          attach_div.appendChild(fdiv);
        }


      } catch (e) {
        console.error(e.stack);
      }
    }

    upload_input.addEventListener("change", submit_func, false);

    wemaile_attach.addEventListener("click", function() {
      upload_input.click();
    });

    let inemailview = false;

    let wemaile_send = wemaile_form.querySelector('button.send');
    wemaile_send.addEventListener("click", async function() {
      try {
        display_loading_overlay();
        var we_resp = await XHR.post(
          '/content-managemer/mailer.io/send',
          { 
            formData: new FormData(attach_form),
            headers: {
              email_to: wemaile_to.value,
              email_subject: wemaile_subject.value,
              email_content: wemaile_content.value,
              eaddr: _this.cur_eaddr,
              boxname: _this.curopenbox.name
            },
            on_progress: function(e) {
              if (e.lengthComputable) {
                console.log((e.loaded / e.total) * 100);
              }
            }
          },
          'access_token'
        );
        console.log(we_resp);

        attach_div.innerHTML = "";
        attach_form.reset();

        wemaile_to.value = '';
        wemaile_subject.value = '';
        wemaile_content.value = '';

        wemaile_form.style.display = "";
        if (inemailview) {
          email_view.style.display = "block";
        } else {
          wemaile.style.display = "";
        }

        hide_loading_overlay();
      } catch (e) {
        console.error(e.stack);
      }
    });

    let wemaile_discard = wemaile_form.querySelector('button.discard');
    wemaile_discard.addEventListener("click", function() {
      wemaile_to.value = '';
      wemaile_subject.value = '';
      wemaile_content.value = '';

      wemaile_form.style.display = "";
      if (inemailview) {
        email_view.style.display = "block";
        inemailview = false;
      } else {
        wemaile.style.display = "";
      }
    });

    wemaile.addEventListener("click", function() {
      wemaile_form.style.display = "flex";
      wemaile.style.display = "none";
    });

    let email_view = document.getElementById("email_view");

    _this.reply_cb = function(e, subject, msg, to) {
      inemailview = true;
      wemaile_form.style.display = "flex";
      email_view.style.display = "none";

      console.log("TO", to);
      wemaile_to.value = to.includes('<') ? to.match(/(<).*?(>)/g)[0].substring(1).slice(0, -1) : to;
      wemaile_subject.value = 'RE: '+subject;
      wemaile_content.value = '\n\n<div>'+msg+'<div>';
    };

    _this.forward_cb = function(e, subject, msg, from) {
      inemailview = true;
      wemaile_form.style.display = "flex";
      email_view.style.display = "none";

      wemaile_subject.value = 'FWD: '+subject;
      wemaile_content.value = '<h3>FROM: '+from+'</h3><div>'+msg+'<div>';
    };


    let page_limit = 10

    let navi_init = true;

    let elem = document.createElement("div");
    let navigator = await ContentNavigator.init({
      div: elem,
      max_index: _this.max_index,
      page_size: page_limit,
    }, async function(index, page_size, max_index, content_navigator) {
      try {
        if (navi_init) {
          await navigator.resize(10, _this.max_index);
          navi_init = false;
        } else {
          if (select_all) select_all.checked = false;
          console.log("navipage");
          mail_page = await MailPage.construct(index*page_limit, page_limit, content_navigator, _this);
          await navigator.resize(10, _this.max_index);
          hide_loading_overlay();
        }
      } catch (e) {
        console.error(e.stack);
      }
    });


    await navigator.resize(10, _this.max_index);
    await navigator.get_page(0, navigator.page_size);

    let displayed_box_btn = false;
    for (let b = 0; b < mailboxes.length; b++) {
      let box_tab_btn = document.createElement("button");
      box_tab_btn.innerHTML = mailboxes[b];
      if (mailboxes[b].toLowerCase() == "inbox") {
        box_tab_btn.classList.add("selected");
        displayed_box_btn = box_tab_btn;
      }

      box_tab_btn.addEventListener("click", async function() {
        try {
          display_loading_overlay();

          wemaile.style.display = "";
          content_elem.style.display = "";

          displayed_box_btn.classList.remove("selected");
          box_tab_btn.classList.add("selected");
          displayed_box_btn = box_tab_btn;

          let allopenbox = false;
          for (let o = 0; o < open_boxes.length; o++) {
            if (open_boxes[o].name == mailboxes[b]) {
              allopenbox = open_boxes[o];
            }
          }


          _this.max_index = await XHR.post("/content-manager/mailer.io", {
            command: "inbox_max",
            addr: _this.cur_eaddr,
            boxname: mailboxes[b]
          }, "access_token");


/*          await XHR.post("/content-manager/mailer.io", {
            command: "open_box",
            name: ,
            addr: _this.cur_eaddr
          }, "access_token");a*/

          if (allopenbox) {
            _this.curopenbox.hide();
            allopenbox.display();
            _this.curopenbox = allopenbox;
          } else {
            _this.curopenbox.hide();
            _this.curopenbox = await open_mailbox(mailboxes[b]);
            _this.curopenbox.display();
            open_boxes.push(_this.curopenbox);
          }
          await navigator.resize(navigator.page_size, _this.max_index);
          await navigator.get_page(0, navigator.page_size);
          hide_loading_overlay();
        } catch (e) {
          console.error(e.stack);
        }
      });

      mboxe.appendChild(box_tab_btn);
    }





    let top_opts = document.createElement("div");
    top_opts.innerHTML = top_opts_html;
    top_opts.classList.add("top_opts");

    navigator.controls.top.parentNode.insertBefore(top_opts, navigator.controls.top);
    await navigator.get_page(0, navigator.page_size);
    await navigator.resize(10, _this.max_index);

    let select_all = top_opts.querySelector("input");
    select_all.addEventListener("click", function() {
      function tr_check(tr, check) {
        let checke = tr.querySelector("input");
        if (check) {
          checke.checked = true;
        } else {
          checke.checked = false;
        }
      }

      for (let t = 0; t < mail_page.tr_list.length; t++) {
        tr_check(mail_page.tr_list[t], select_all.checked);
      }
    });


    function all_checked_trs() {
      let trs = [];
      for (let t = 0; t < mail_page.tr_list.length; t++) {
        if (mail_page.tr_list[t].querySelector("input").checked) trs.push(mail_page.tr_list[t]);
      }
      return trs;
    }

    let delete_selected = top_opts.querySelector("button:nth-child(2)");
    delete_selected.addEventListener("click", async function() {
      try {
        display_loading_overlay();
        let all_trs = all_checked_trs();
        new_email_counter.innerHTML = await mail_page.email_command("delete", all_trs);
        for (let t = 0; t < all_trs.length; t++) {
          await all_trs[t].email_delete({ skip_email_command: true });
        }

        _this.max_index -= all_trs.length;

        await navigator.resize(false, _this.max_index);
        await navigator.get_page(navigator.cur_page.index, navigator.page_size);
      } catch (e) {
        console.error(e.stack);
      }
    });

    let read_selected = top_opts.querySelector("button:nth-child(3)");
    read_selected.addEventListener("click", async function() {
      try {
        display_loading_overlay();
        let all_trs = all_checked_trs();
        new_email_counter.innerHTML = await mail_page.email_command("read", all_trs);
        for (let t = 0; t < all_trs.length; t++) {
          await all_trs[t].email_read({ skip_email_command: true });
        }
        hide_loading_overlay();
      } catch (e) {
        console.error(e.stack);
      }
    });

    let unread_selected = top_opts.querySelector("button:nth-child(4)");
    unread_selected.addEventListener("click", async function() {
      try {
        display_loading_overlay();
        let all_trs = all_checked_trs();
        new_email_counter.innerHTML = await mail_page.email_command("unread", all_trs);
        for (let t = 0; t < all_trs.length; t++) {
          await all_trs[t].email_unread({ skip_email_command: true });
        }
        hide_loading_overlay();
      } catch (e) {
        console.error(e.stack);
      }
    });


    content_elem.appendChild(elem);


    console.log("CONNECTING TO", server_addr+'/mailer.io');
    import socket from 'socket.io-client'(server_addr, {
      path: '/mailer.io',
      transportOptions: {
        polling: {
          extraHeaders: {
            "Authorization": await FlyAuth.authentials("admin_csrf_token"),
           // 'Fingerprint': JSON.stringify(await fingerprint.get())
          }
        }
      }
    });

    socket.on('error', (error) => {
      console.error(new Error(error.status+" "+error.msg));
    });

    socket.on('disconnect', function(){
      console.log("DISCONNETED");
    });
/*
    socket.on('connect', function(){
      console.log("SOCKET CONNETED");
      socket.on("new_mail", async function(data) {
        try {
          if (_this.list_view) display_loading_overlay();
          new_email_counter.innerHTML = parseInt(new_email_counter.innerHTML) + data;
          let get_page_index = navigator.cur_page.index;

          _this.max_index += data;
          await navigator.resize(10, _this.max_index);
          if (get_page_index == 0) await navigator.get_page(0, navigator.page_size); 
          if (_this.list_view) hide_loading_overlay();
        } catch (e) {
          console.error(e.stack);
        }
      });
    });*/

  } catch (e) {
    console.error(e.stack);
  }
})();
