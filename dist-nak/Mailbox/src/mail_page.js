
import XHR from 'core/utils/xhr_async.js';

import body_html from './mail_page.html';
import email_tr_html from './email_tr.html';

const listelem = document.getElementById("content");
const emailelem = document.getElementById("email_view");
const emailelem_back = emailelem.querySelector("button");

let new_email_counter = document.getElementById("new_email_counter");

export default class {
  static async construct(offset, page_limit, navigator, mailer) {
    try {
      let _this = new module.exports();
      _this.mailer = mailer;

      emailelem_back.addEventListener("click", function() {
        _this.mailer.list_view = true;
        listelem.style.display = "";
        emailelem.style.display = "";
      });

      console.log("cboxname",mailer.curopenbox);
      let emails = await XHR.post("/content-manager/mailer.io", {
        command: "inbox",
        offset: offset,
        limit: page_limit,
        addr: mailer.cur_eaddr,
        boxname: mailer.curopenbox.name
      }, "access_token");
      console.log(emails);

      navigator.display.innerHTML = body_html;
      let etable = navigator.display.querySelector("table");

      let cur_reply_listener = false;
      let cur_forward_listener = false;

      for (let e = 0; e < emails.length; e++) {
        let tre = document.createElement("tr");
        tre.email_uid = emails[e].uid;

        tre.innerHTML = email_tr_html;

        tre.email_delete = async function(event) {
          try {
            display_loading_overlay();
            if (!event.skip_email_command) {
              new_email_counter.innerHTML = await _this.email_command("delete", [tre]);


              mailer.max_index--;
              await navigator.resize(false, mailer.max_index);
              await navigator.get_page(navigator.cur_page.index, navigator.page_size);
            }
            hide_loading_overlay();
          } catch (e) {
            console.error(e.stack);
          }
        }
        let star_btn = tre.querySelector("td:nth-child(2) button");
        star_btn.addEventListener("click", async function(event) {
          try {
            await _this.email_command("star", [tre]);

            unstar_btn.style.display = "inline-block";
            star_btn.style.display = "none";
          } catch (e) {
            console.error(e.stack);
          }
        });

        let unstar_btn = tre.querySelector("td:nth-child(2) button:last-child");
        unstar_btn.addEventListener("click", async function(event) {
          try {
            await _this.email_command("unstar", [tre]);

            unstar_btn.style.display = "";
            star_btn.style.display = "";
          } catch (e) {
            console.error(e.stack);
          }
        });

        let delete_btn = tre.querySelector("td:last-child button");
        delete_btn.addEventListener("click", tre.email_delete);

        tre.email_unread = async function(event) {
          try {
            if (!event.skip_email_command) new_email_counter.innerHTML = await _this.email_command("unread", [tre]);

            tre.classList.remove("seen");
            unread_btn.style.display = "none";
            read_btn.style.display = "inline-block";
          } catch (e) {
            console.error(e.stack);
          }
        }
        let unread_btn = tre.querySelector("td:last-child button:nth-child(2)");
        unread_btn.addEventListener("click", tre.email_unread);

        tre.email_read = async function(event) {
          try {
            if (!event.skip_email_command) new_email_counter.innerHTML = await _this.email_command("read", [tre]);

            tre.classList.add("seen");
            unread_btn.style.display = "";
            read_btn.style.display = "";
          } catch (e) {
            console.error(e.stack);
          }
        }
        let read_btn = tre.querySelector("td:last-child button:last-child");
        read_btn.addEventListener("click", tre.email_read);


        if (emails[e].flags.includes("\\Seen")) {
          tre.classList.add("seen");
        } else {
          unread_btn.style.display = "none";
          read_btn.style.display = "inline-block";
        }

        if (emails[e].flags.includes("\\Flagged")) {
          unstar_btn.style.display = "inline-block";
          star_btn.style.display = "none";
        }

        let td_from = tre.querySelector("td:nth-child(3)");
        td_from.innerText = emails[e].head.from[0]+" "+emails[e].head.subject[0]+" "+emails[e].head.date[0];
        td_from.addEventListener("click", async function() {
          try {
            _this.mailer.list_view = false;

            unread_btn.style.display = "";
            read_btn.style.display = "";
            tre.classList.add("seen");

            listelem.style.display = "none";
            emailelem.style.display = "block";

            let email = await XHR.post("/content-manager/mailer.io", {
              command: "email",
              uid: emails[e].uid,
              addr: mailer.cur_eaddr,
              boxname: mailer.curopenbox.name
            }, "access_token");

            console.log(email);

            let eh1 = emailelem.querySelector("h1");
            eh1.innerText = emails[e].head.from[0]+" "+emails[e].head.subject[0]+" "+emails[e].head.date[0];

            let content_div = emailelem.querySelector("div.email_body");
            content_div.innerHTML = decodeURIComponent(email);
                  
            let ereply_btn = emailelem.querySelector('.email_head button.reply');
            if (cur_reply_listener) {
              ereply_btn.removeEventListener("click", cur_reply_listener);
              cur_reply_listener = false;
            }
            cur_reply_listener = function(event) {
              mailer.reply_cb(event, emails[e].head.subject[0], email, emails[e].head.from[0]);
            }
            ereply_btn.addEventListener("click", cur_reply_listener);


            let eforward_btn = emailelem.querySelector('.email_head button.forward');
            if (cur_forward_listener) {
              eforward_btn.removeEventListener("click", cur_forward_listener);
              cur_forward_listener = false;
            }
            cur_forward_listener = function(event) {
              mailer.forward_cb(event, emails[e].head.subject[0], email, emails[e].head.from[0]);
            };
            eforward_btn.addEventListener("click", cur_forward_listener);

            
          } catch (e) {
            console.error(e.stack);
          }
        });

        etable.appendChild(tre);

        _this.tr_list.push(tre);
      }
      

      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {
    this.tr_list = []
  }

  async email_command(cmd, trs) {
    try {
      let uids = [];
      for (let t = 0; t < trs.length; t++) {
        uids[t] = trs[t].email_uid;
      }

      let mailer = this.mailer;

      let resp = await XHR.post("/content-manager/mailer.io", {
        command: cmd,
        uids: uids,
        addr: mailer.cur_eaddr,
        boxname: mailer.curopenbox.name
      }, "access_token");
      console.log(resp);
      return resp;
    } catch (e) {
      console.error(e.stack);
    }
  }
}
