
(async () => {
  try {
    await require("globals/header.js").construct();

    import XHR from 'core/utils/xhr_async.js';
    import ContentNavigator from 'core/content_navigator/index.js';
    import Page from './page.js';
    import Post from './post.js';

    
    let category_block = document.getElementById("category_block");
    let categories = await XHR.get('/content-manager/posts', {
      command: "all_categories"
    });


    let incateg = document.createElement("input");
    incateg.type = "text";
    category_block.appendChild(incateg);
    let incateg_submit = document.createElement("button");
    incateg_submit.innerHTML = "Add";
    incateg_submit.addEventListener("click", async function(e) { 
      try {
        let ncateg_res = await XHR.post('/content-manager/posts',       {
          command: "add_category",
          name: incateg.value
        }, "access_token");
        if (ncateg_res.err) {
          alert(ncateg_res.err);
        } else {
          categories.push({ name: incateg.value });
          add_category(incateg.value);
        }
      } catch (err) {
        console.log(err.stack);
      }
    });

    function add_category(name) {
      let categ = document.createElement("div");
      categ.innerHTML = name;
      incateg.value = "";

      let del_categ = document.createElement("button");
      del_categ.innerHTML = "x";
      del_categ.addEventListener("click", async function(e) { 
        try {
          if (confirm("Are you sure you want to delete this category?")) {
            let ncateg_res = await XHR.post('/content-manager/posts',       {
              command: "del_category",
              name: name
            }, "access_token");
            category_block.removeChild(categ);
            categories.splice(categories.indexOf(name), 1);
          }
        } catch (err) {
          console.log(err.stack);
        }
      });
      categ.appendChild(del_categ);
      category_block.insertBefore(categ, incateg);
    }

    for (let c = 0; c < categories.length; c++) {
      add_category(categories[c].name);
    }

    category_block.appendChild(incateg_submit);

    let max_index = await XHR.get('/content-manager/posts', {
      command: "max_index"
    });


    console.log("MAX INDEX", max_index);


    let navigator = await ContentNavigator.init({
      page_size: 10,
      max_index: max_index
    }, async function(index, page_size, max_index, content_navigator) {
      console.log("navi_cb", content_navigator);
      content_navigator.display.innerHTML = "";
      let page = await Page.init(
        index, page_size, max_index, content_navigator
      ); // TODO: remove max_pages argument as it is never used nor will be...
      hide_loading_overlay();
      return page;
    });
    navigator.categories = categories;


    let post_list = navigator.display;
    
    let div = document.body.querySelector(".npost_block");
    let post_editor = div.querySelector(".post_editor"),
    title_input = div.querySelector(".post_title_input"),
    tags_input = div.querySelector(".post_tags_input"),
    submit_input = div.querySelector(".post_submit_input"),
    post_display_title = div.querySelector(".post_display_title"),
    post_display = div.querySelector(".post_display"),
    jodit_area = div.querySelector(".jodit"),
    npost_categ = post_editor.querySelector('select')





    title_input.addEventListener("input", function(e) {
      post_display_title.innerHTML = title_input.value;
    });

    jodit_area.id = "jodit";
    let jodit = new Jodit('#jodit', {
      toolbarAdaptive: false
    });
    jodit_area.id = "";

    jodit.events.on("change", function() {
      post_display.innerHTML = jodit.value;
    });

    let this_class = this;

    submit_input.addEventListener("click", async function(e) {
      console.log("new_post");
      let tags; 
      if (tags_input) {
        tags = tags_input.value.split(" ");
      }
      var data = {
        command: "create",
        post: {
          title: title_input.value,
          content: jodit.value,
          tags: tags,
          categories: [npost_categ.value]
        }
      }

      let res = await XHR.post('/content-manager/posts', data, "access_token");
      console.log("posts_res", res);

      if (res.err) {
        console.error(res.err);
      } else {
        reset_memory();
        new_post_button.style.display = "block";
        post_editor.style.display = "none";
        title_input.value = "";
        jodit.value = "";
        tags_input.value = "";
        post_editor.querySelector('select').value = "Category";
        await navigator.cur_page.client_page.resize();
      }
    });

    let dcateg = document.createElement("option");
    dcateg.innerHTML = "Category";
    dcateg.selected = true;
    dcateg.disabled = true;
    post_editor.querySelector('select').appendChild(dcateg);
    for (let c = 0; c < categories.length; c++) {
      let categ = document.createElement("option");
      categ.innerHTML = categories[c].name;
      categ.value = categories[c].name;
      post_editor.querySelector('select').appendChild(categ);
    }

    var new_post_button = div.querySelector(".new_post_button");
    new_post_button.addEventListener("click", function(e) {

      new_post_button.style.display = "none";
      post_editor.style.display = "block";
    });
    div.classList.add('posts');


    // memorize +
    const mellisuga_cms_new_post = "mellisuga_cms_new_post_"

    function update_new_post_storage(e, which) {
      let existing_post = window.localStorage.getItem(mellisuga_cms_new_post+which);
      window.localStorage.setItem(mellisuga_cms_new_post+which, e.target.value);

    }


    title_input.value = window.localStorage.getItem(mellisuga_cms_new_post+"title");
    title_input.addEventListener("input", function(e) {
      update_new_post_storage(e, "title");
    });

    tags_input.value = window.localStorage.getItem(mellisuga_cms_new_post+"tags");
    tags_input.addEventListener("input", function(e) {
      update_new_post_storage(e, "tags");
    });


    jodit.value = window.localStorage.getItem(mellisuga_cms_new_post+"content") || "";
    jodit.events.on("change", function() {
      let e = {
        target: {
          value: jodit.value
        }
      }
      update_new_post_storage(e, "content");
    });

    function reset_memory() {
      title_input.value = "",
      window.localStorage.removeItem(mellisuga_cms_new_post+"title");

      tags_input.value = "",
      window.localStorage.removeItem(mellisuga_cms_new_post+"tags");

      jodit.value = "",
      window.localStorage.removeItem(mellisuga_cms_new_post+"content");
    }

    // memorize -

    if (title_input.value || tags_input.value) {
      new_post_button.click();
    }



    await navigator.select_page(0);
    hide_loading_overlay();
  } catch (e) {
    console.error(e.stack);
  }
})();


