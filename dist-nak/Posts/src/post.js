

import XHR from 'core/utils/xhr_async.js';

import html from './post.html';
import edit_html from './edit.html';

export default class {
  constructor(obj, navigator) {
    this.element = document.createElement('div');

    this.navigator = navigator;

    this.display(obj);
  }

  edit(obj) {
    var div = this.element;

    div.innerHTML = edit_html;

    var post_editor = div.querySelector(".post_editor");
    var title_input = div.querySelector(".post_title_input");
    title_input.value = obj.title;
    var tags_input = div.querySelector(".post_tags_input");
    if (tags_input) {
      tags_input.value = obj.tags;
    }
    let submit_input = div.querySelector(".post_submit_input"),
    cancel_input = div.querySelector(".post_cancel_input"),
    post_display_title = div.querySelector(".post_display_title"),
    post_display = div.querySelector(".post_display"),
    jodit_area = div.querySelector(".jodit");

    post_display_title.innerHTML = obj.title;
    post_display.innerHTML = obj.content;

    var this_class = this;
    cancel_input.addEventListener("click", function(e) {
      this_class.display(obj);
    });

    title_input.addEventListener("input", function(e) {
      post_display_title.innerHTML = title_input.value;
    });
    
    jodit_area.id = "jodit";
    let jodit = new Jodit('#jodit', {
      toolbarAdaptive: false
    });
    jodit_area.id = "";
    
    jodit.value = obj.content;

    jodit.events.on("change", function() {
      post_display.innerHTML = jodit.value;
    });

    post_editor.style.display = "block";


    console.log("postOJb", obj);
    submit_input.addEventListener("click", async function(e) {
      let tags;
      if (tags_input) {
        tags = tags_input.value.split(" ");
      }


      var data = {
        command: "edit",
        post: {
          id: obj.id,
          title: title_input.value,
          content: jodit.value,
          tags: tags,
          categories: [npost_categ.value]
        }
      }

      console.log(data);
      console.log("edit post");
      const response = await XHR.post('/content-manager/posts', data, "access_token");
      console.log("response", response);
      if (response === "success") {
        console.log("Post successfuly edited!");
        obj.title = title_input.value;
        obj.content = jodit.value;
        obj.tags = tags;
        obj.edit_date = Date.now();
//        this_class.make_first();
        div.innerHTML = html;
        this_class.display(obj);
      }
    });
    let categories = this.navigator.navigator.categories;
    let dcateg = document.createElement("option");
    dcateg.innerHTML = "Category";
    dcateg.selected = true;
    dcateg.disabled = true;
    post_editor.querySelector('select').appendChild(dcateg);
    for (let c = 0; c < categories.length; c++) {
      let categ = document.createElement("option");
      categ.innerHTML = categories[c].name;
      categ.value = categories[c].name;
      post_editor.querySelector('select').appendChild(categ);
    }


    let npost_categ = post_editor.querySelector('select')
    if (obj.categories && obj.categories.length > 0) {
      npost_categ.value = obj.categories[0]
    }
  }

  make_first() {
    var parent = this.element.parentNode;
    parent.removeChild(this.element);
    parent.insertBefore(this.element, parent.firstChild);
  }

  display(obj) {
    this.element.innerHTML = html;
    this.element.classList.add('post_element');

    var title = this.element.querySelector('.post_title');
    title.innerHTML = obj.title;
    title.classList.add('post_display_title');

    let published = this.element.querySelector('.post_published');
    this.published_text = lang.published;
    const pdate = new Date(parseInt(obj.publish_date));
    published.innerHTML = this.published_text+pdate.getFullYear()+"-"+pdate.getMonth()+
    "-"+pdate.getDate()+" "+pdate.getHours()+":"+pdate.getMinutes();

    const edate = new Date(parseInt(obj.edit_date));
    
    if (!isNaN(edate)) {
      let edited = this.element.querySelector('.post_edited');
      this.edited_text = lang.edited;
      edited.innerHTML = this.edited_text+edate.getFullYear()+"-"+edate.getMonth()+
      "-"+edate.getDate()+" "+edate.getHours()+":"+edate.getMinutes();
    }

    var edit_btn = this.element.querySelector('.post_edit_btn');

    var this_class = this;
    edit_btn.addEventListener("click", function(e) {
      this_class.edit(obj);
    });
    console.log("POST OBJ", obj);

    var del_btn = this.element.querySelector('.post_del_btn');
    del_btn.addEventListener("click", async function(e) {
      let popup = document.getElementById('del_popup');
      popup.style.display = "block";
  
      var ybtn = popup.querySelector(".del_yes");
      ybtn.addEventListener("click", async (e) => {
        const response = await XHR.post('/content-manager/posts', {
          command: "delete", ids: [obj.id]
        }, "access_token");
        if (response == "success") {
          if (this_class.element.parentNode) {
            this_class.element.parentNode.removeChild(this_class.element); 
          }
          popup.style.display = "";
          await this_class.navigator.resize();
        }
      });

      var nbtn = popup.querySelector(".del_no");
      nbtn.addEventListener("click", (e) => {
        popup.style.display = "";
      });
    });

    if (obj.categories) {
      let categ_div = document.createElement("div");
      categ_div.classList.add("post_category");
      categ_div.innerHTML = "Categories: ";
      for (let c = 0; c < obj.categories.length; c++) {
        if (c > 0) categ_div.innerHTML += ", ";
        categ_div.innerHTML += obj.categories[c];
      }
      if (categ_div.innerHTML == "Categories: Category") categ_div.innerHTML = "No category";
      this.element.appendChild(categ_div);
    }
    var content = this.element.querySelector('.post_content');

    var content_str = obj.content;
/*    var max_length = 1024;
    if(content_str.length > max_length) {
      content_str = content_str.substring(0,max_length)+'...';
    }*/
    content.innerHTML = content_str;
    content.classList.add('post_display');
    deheight_imgs(content);
  }
}

function deheight_imgs(element) {
  let imgs = element.querySelectorAll('img');
  for (let i = 0; i < imgs.length; i++) {
    let img = imgs[i];
    img.style.height = "";
  }
}
