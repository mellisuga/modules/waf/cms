
import XHR from 'core/utils/xhr_async.js';
import CodeMirror from 'core/codemirror.ui/index.js';
import Account from './account.js';
let div = document.getElementById('admin_accounts_ui');
/*
let accounts_array = [];

let accounts_list = div.querySelector(".admin_accounts_ui_list");

let new_account_form = div.querySelector("#admin_accounts_ui_new_item_input");

let new_acc_email_input = new_account_form.querySelector('input[name="email"]');
let new_acc_password_input = new_account_form.querySelector('input[name="password"]');

let new_acc_super_input = new_account_form.querySelector('input[name="super"]');

var json_edit = div.querySelector(".new_json_edit");
*/
(async function() {
  await require("globals/header.js").construct();
  let usr_details = await XHR.post('/admin-api', {
    command: "details"
  }, 'access_token');

  let yacc = document.getElementById("yacc");
  let yemail = document.createElement("div");
  yemail.innerHTML = usr_details.email;
  yacc.appendChild(yemail); 

  let cypwd_block = document.createElement("div");
  let iypwd = document.createElement("input");
  iypwd.type = "password";
  iypwd.name = "upwd";
  iypwd.placeholder = "Your Password";
  cypwd_block.appendChild(iypwd);

  let inpwd1 = document.createElement("input");
  inpwd1.type = "password";
  inpwd1.name = "npwd1";
  inpwd1.placeholder = "New Password";
  cypwd_block.appendChild(inpwd1);

  let inpwd2 = document.createElement("input");
  inpwd2.type = "password";
  inpwd2.name = "npwd2";
  inpwd2.placeholder = "Repeat Your New Password";
  cypwd_block.appendChild(inpwd2);

  let cypwd_submit = document.createElement("button");
  cypwd_submit.innerHTML = "Proceed";
  cypwd_submit.addEventListener("click", async function(e) {
    try {
      let cpwd_res = await XHR.post("/admin-api", {
        command: "change-password",
        upwd: iypwd.value,
        npwd1: inpwd1.value,
        npwd2: inpwd2.value
      }, 'access_token');
      if (cpwd_res.err) {
        alert(cpwd_res.err);
      } else {
        alert("Your password has been changed!");
        iypwd.value = "";
        inpwd1.value = "";
        inpwd2.value = "";
        yacc.removeChild(cypwd_block);
        yacc.appendChild(cypwd);
        location.reload(true);
      }
    } catch (err) {
      console.error(err.stack);
    }
  });
  cypwd_block.appendChild(cypwd_submit);

  let cypwd_cancel = document.createElement("button");
  cypwd_cancel.innerHTML = "Cancel";
  cypwd_cancel.addEventListener("click", function(e) {
    yacc.removeChild(cypwd_block);
    yacc.appendChild(cypwd);
  });
  cypwd_block.appendChild(cypwd_cancel);

  let cypwd = document.createElement("button");
  cypwd.innerHTML = "Change password";
  yacc.appendChild(cypwd);

  cypwd.addEventListener("click", function(e) {
    yacc.removeChild(cypwd);
    yacc.appendChild(cypwd_block);
  });

/*
  let accounts = await XHR.post('/mellisuga/admin_accounts.io', {
    command: "all"
  }, 'access_token');

  for (let p = 0; p < accounts.length; p++) {
    accounts[p].cfg = JSON.stringify(accounts[p].cfg);
    var account = new Account(accounts[p]);
    new_account_form.parentNode.insertBefore(account.element, new_account_form);
    accounts_array.push(account);
  }*/
})();

/*

let new_account_button = div.querySelector("#admin_accounts_ui_new_item");
let html_editor = undefined;

new_account_button.addEventListener("click", function(e) {

  new_account_button.style.display = "none";
  new_account_form.style.display = "";

  new_acc_email_input.value = "";
  new_acc_password_input.value = "";


  html_editor = new CodeMirror('', 'js', false);
  json_edit.appendChild(html_editor.element);
  html_editor.cm.setSize(500, 100);
  html_editor.cm.refresh();
});

let new_acc_submit = new_account_form.querySelector('button[name="submit"]');
new_acc_submit.addEventListener("click", async function(e) {
  var data = {
    email: new_acc_email_input.value,
    password: new_acc_password_input.value
  };

  if (new_acc_super_input.checked) {
    data.super = true;
  } else {
    data.cfg = html_editor.cm.getValue();
  }
  const response = await XHR.post('/mellisuga/admin_accounts.io', {
    command: "add",
    data: data
  }, "access_token");
  if (response === "success") {
    var account = new Account(data);
    new_account_form.parentNode.insertBefore(account.element, new_account_form);
    new_account_button.style.display = "block";
    new_account_form.style.display = "none";
  } else {
    console.log(response);
  }
});

let new_acc_cancel = new_account_form.querySelector('button[name="cancel"]');
new_acc_cancel.addEventListener("click", function(e) {
  new_account_button.style.display = "block";
  new_account_form.style.display = "none";
});
*/

function refresh() {
  for (var a = 0; a < accounts_array.length; a++) {
    console.log("r", a);
    accounts_array[a].html_editor.cm.refresh();
  }
}


 hide_loading_overlay();
